<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// products api
Route::get('products', 'APIProductController@index')->name('api.products.index');
Route::get('products/{product}', 'APIProductController@show')->name('api.products.show');
Route::post('products', 'APIProductController@store')->name('api.products.store');
Route::put('products/{product}', 'APIProductController@update')->name('api.products.update');
Route::delete('products/{product}', 'APIProductController@delete')->name('api.products.delete');

// retailers api

Route::get('retailers', 'APIRetailerController@index')->name('api.retailers.index');
Route::get('retailers/{retailer}', 'APIRetailerController@show')->name('api.retailers.show');
Route::post('retailers', 'APIRetailerController@store')->name('api.retailers.store');
Route::put('retailers/{retailer}', 'APIRetailerController@update')->name('api.retailers.update');
Route::delete('retailers/{retailer}', 'APIRetailerController@delete')->name('api.retailers.delete');



Route::post('/send-email/{id}', 'SendEmailController@sendemail')->name('send-email');
