<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('shop.index');
})->name('home');



Route::get('/products', 'ProductController@index')->name('products.index');
Route::get('/products/{product}', 'ProductController@show')->name('products.show');
Route::get('/product/create', 'ProductController@new_product')->name('products.new');
Route::get('/product/{product}/edit', 'ProductController@edit_product')->name('products.edit');


Route::get('/retailers', 'RetailerController@index')->name('retailers.index');
Route::get('/retailers/{retailer}', 'RetailerController@show')->name('retailers.show');
Route::get('/retailer/create', 'RetailerController@new_retailer')->name('retailers.new');
Route::get('/retailer/{retailer}/edit', 'RetailerController@edit_retailer')->name('retailer.edit');


Route::group(['prefix' => 'user'], function() {
	Route::group(['middleware' => 'guest'], function () {
		Route::get('/signup', 'UserController@getSignUp')->name('user.signup');
		Route::post('/signup', 'UserController@postSignUp')->name('user.signup');

		Route::get('/signin', 'UserController@getSignIn')->name('user.signin');
		Route::post('/signin', 'UserController@postSignIn')->name('user.signin');
	});

	Route::group(['middleware' => 'auth'], function () {
		Route::get('/profile', 'UserController@getProfile')->name('user.profile');
		Route::get('/logout', 'UserController@getLogout')->name('user.logout');	
	});
});