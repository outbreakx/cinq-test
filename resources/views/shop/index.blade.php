@extends('layouts.master')

@section('title')
	Something
@endsection


@section('content')
<div class="container">
	<h1 class="text-center">This is a test app.</h1>
	<div class="form-row text-center">
    	<div class="col-12">
        	<a class="btn btn-success text-center" href="https://github.com/cinqtechnologies/php-test">Test instructions</a>
        </div>
 	</div>
</div>
@endsection