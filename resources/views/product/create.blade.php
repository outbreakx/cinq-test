@extends('layouts.master')

@section('title')
Create Page Product
@endsection


@section('content')
<div class="container">
    <h2 class="text-center">Create new product</h2>
    <form method="POST" action="{{ route('api.products.store') }}" enctype="multipart/form-data" id="form-product">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="name">Price:</label>
            <input class="form-control" type="number" value="0.0" id="price" name="price" step=".10">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="veiculo">Retailers</label>
            <select required class="form-control" name="retailer_id" id="retailer_id">
                @foreach ($retailers as $retailer)
                	<option value="{{ $retailer->id }}">{{ $retailer->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group control-group increment">
            <input type="file" name="filename[]" id="filename[]" class="form-control">
            <div class="input-group-btn">
                <button class="btn btn-success" type="button"><i class="fas fa-plus"></i></button>
            </div>
        </div>
        <div class="form-group" style="margin-top: 1%;">
            <button class="btn btn-primary" id="create-product">Submit</button>
        </div>
    </form>
</div>
@endsection


@section('styles')
<style type="text/css">
	
</style>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var url_create_product = '{!! route('api.products.store') !!}';
    $("#create-product").click(function(e) {
        e.preventDefault();
        
        var formData = new FormData($("#form-product")[0]);
        $.ajax({
            type: 'POST',
            url: url_create_product,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            success: function(result) {
                alert('it has been created!');
            },
            error: function(result) {

            }
        });
        return false;

    });
});
</script>
@endsection