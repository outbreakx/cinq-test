@extends('layouts.master')

@section('title')
Edit Page Product
@endsection


@section('content')
<div class="container">
    <h2 class="text-center">Edit new product</h2>
    <form method="PUT" action="{{ route('api.products.update', $product->id) }}" id="form-product">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}">
        </div>
        <div class="form-group">
            <label for="name">Price:</label>
            <input class="form-control" type="number" id="price" value="{{ $product->price }}" name="price" step=".10">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3">{{ $product->description }}</textarea>
        </div>
        <div class="form-group" style="margin-top: 1%;">
            <button class="btn btn-primary" id="create-product">Submit</button>
        </div>
    </form>
</div>
@endsection


@section('styles')
<style type="text/css">
	
</style>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var url_create_product = '{!! route('api.products.update', $product->id) !!}';
    $("#create-product").click(function(e) {
        e.preventDefault();
        
        var data = $("#form-product").serialize();
        $.ajax({
            type: 'PUT',
            url: url_create_product,
            data: data,

            success: function(result) {
                alert('it has been updated!');
            },
            error: function(result) {

            }
        });
        return false;

    });
});
</script>
@endsection