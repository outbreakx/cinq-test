@extends('layouts.master')

@section('title')
	Product Page
@endsection


@section('content')
<div class="container" style="margin-top: 5%;">
	<div class="row text-center">
		<div class="col-sm">
			<img src="{{ asset($retailer->logo->filename) }}">
		</div>
		<div class="col-sm">
			<a class="btn btn-success" href="{{ route('retailer.edit', $retailer->id) }}" class="cart-btn">Edit Retailer</a>
			<div class="retailer-info">
				<h3>{{ $retailer->name }}</h3>
			</div>
			<hr>
			<div class="retailer-description">
				<h3>{{ $retailer->description }}</h3>
			</div>
			<div class="retailer-website">
				<a class="btn btn-success" href="{{ $retailer->website }}">Visit website</a>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col text-center">
			<h2>My products</h2>
		</div>
	</div>

	@foreach($retailer->products->chunk(4) as $productChunk)
    <div class="row" style="margin-bottom: 1%; margin-top: 3%;">
        @foreach($productChunk as $product)
        <div class="col col-md-auto">
            <div class="card">
            <img class="card-img-top" src="{{ asset($retailer->logo->filename ) }}" alt="Card image">
            <div class="card-body">
                <h4 class="card-title">{{ $product->name}}</h4>
                <p class="card-text">{{ $product->description }}</p>
                <span>Price: {{ $product->price }}</span>
                <hr>
                <a class="btn btn-success" href="{{ route('products.show', $product->id)}}">Visit</a>
            </div>
        </div>
        </div>
        @endforeach
    </div>
    @endforeach
</div>
@endsection



@section('styles')
<style type="text/css">
	.retailer-info {
		margin-top: 1%;
	}
	.retailer-description {
		font-size: 12px;
    	color: #358ED7;
    	letter-spacing: 1px;
	}
	.retailer-website {
	}
</style>
@endsection


@section('scripts')

@endsection