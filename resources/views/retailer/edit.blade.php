@extends('layouts.master')

@section('title')
Edit Page Retailer
@endsection


@section('content')
<div class="container">
    <h2 class="text-center">Edit new retailer</h2>
    <form method="PUT" action="{{ route('api.retailers.update', $retailer->id) }}" id="form-retailer">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $retailer->name }}">
        </div>
        <div class="form-group">
            <label for="name">Website:</label>
            <input type="text" class="form-control" id="website" name="website" value="{{ $retailer->website }}">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3">{{ $retailer->description }}</textarea>
        </div>
        <div class="form-group" style="margin-top: 1%;">
            <button class="btn btn-primary" id="create-product">Submit</button>
        </div>
    </form>
</div>
@endsection


@section('styles')
<style type="text/css">
	
</style>
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var url_create_product = '{!! route('api.retailers.update', $retailer->id) !!}';
    $("#create-product").click(function(e) {
        e.preventDefault();
        
        var data = $("#form-retailer").serialize();
        $.ajax({
            type: 'PUT',
            url: url_create_product,
            data: data,

            success: function(result) {
                alert('it has been updated!');
            },
            error: function(result) {

            }
        });
        return false;

    });
});
</script>
@endsection