@extends('layouts.master')

@section('title')
	Retailer Page
@endsection


@section('content')
<div class="container">
    @foreach($retailers->chunk(4) as $retailerChunk)
    <div class="row" style="margin-bottom: 1%; margin-top: 3%;">
        @foreach($retailerChunk as $retailer)
        <div class="col">
            <div class="card">
            <img class="card-img-top" src="{{ asset($retailer->logo->filename ) }}" alt="Card image">
            <div class="card-body">
                <h4 class="card-title">{{ $retailer->name}}</h4>
                <p class="card-text">{{ $retailer->description }}</p>
                <a href="{{ $retailer->website }}" class="btn btn-primary">Website</a>
                <a href="{{ route('retailers.show', $retailer->id) }}" class="btn btn-primary">Profile</a>
            </div>
        </div>
        </div>
        @endforeach
    </div>
    @endforeach
</div>


@endsection


@section('styles')
<style type="text/css">

</style>
@endsection