@extends('layouts.master')

@section('title')
	Create Page Retailer

@endsection


@section('content')
<div class="container">
    <h2 class="text-center">Create new retailer</h2>
    <form method="POST" action="{{ route('api.retailers.store') }}" enctype="multipart/form-data" id="form-retailer">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="form-group">
            <label for="name">Website:</label>
            <input type="text" class="form-control" id="website" name="website">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="veiculo">User</label>
            <select required class="form-control" name="user_id" id="user_id">
                @foreach ($users as $user)
                	<option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group control-group increment">
            <input type="file" name="filename[]" id="filename[]" class="form-control">
            <div class="input-group-btn">
                <button class="btn btn-success" type="button"><i class="fas fa-plus"></i></button>
            </div>
        </div>
        <div class="form-group" style="margin-top: 1%;">
            <button class="btn btn-primary" id="create-retailer">Submit</button>
        </div>
    </form>
</div>
@endsection


@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    var url_create_retailer = '{!! route('api.retailers.store') !!}';
    $("#create-retailer").click(function(e) {
        e.preventDefault();
        var formData = new FormData($("#form-retailer")[0]);
        $.ajax({
            type: 'POST',
            url: url_create_retailer,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            success: function(result) {
                alert('it has been created!');
            },
            error: function(result) {

            }
        });
        return false;

    });
});
</script>
@endsection