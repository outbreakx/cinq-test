@extends('layouts.master')

@section('title')
	Register Page
@endsection


@section('content')
<div class="row justify-content-md-center">
    <div class="col-md-4 col-md-offset-4">
        <h1 style="text-align: center;">Sign Up</h1>
        @if ($errors->any())
        	<div class="alert alert-danger">
     			@foreach ($errors->all() as $error)
         			<div>{{$error}}</div>
     			@endforeach
     		</div>
 		@endif
        <form action="{{ route('user.signup') }}" method="post">
        	<div class="form-group">
                <label for="username">Username</label>
                <input type="text" id="username" name="username" class='form-control'>
            </div>
            <div class="form-group">
                <label for="email">E-Mail</label>
                <input type="text" id="email" name="email" class='form-control'>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="password" class='form-control'>
            </div>
            <button type="submit" class="btn btn-primary form-control" >Sign Up</button>
            {{ csrf_field()}}
        </form>
    </div>
</div>
@endsection