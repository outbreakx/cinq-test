@extends('layouts.master')

@section('title')
	Signin Page
@endsection


@section('content')

<div class="row justify-content-md-center">
    <div class="col-md-4 col-md-offset-4">
        <h1 style="text-align: center;">Sign In</h1>
        @if ($errors->any())
        	<div class="alert alert-danger">
     			@foreach ($errors->all() as $error)
         			<div>{{$error}}</div>
     			@endforeach
     		</div>
 		@endif
        <form action="{{ route('user.signin') }}" method="post">
            <div class="form-group">
                <label for="email">E-Mail</label>
                <input type="text" id="email" name="email" class='form-control'>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="password" class='form-control'>
            </div>
            <div class="form-check">
            	<input type="hidden" name='rememberme' id="rememberme" value="no">
    			<input type="checkbox" class="form-check-input" id="rememberme2">
    			<label class="form-check-label" for="rememberme2">Remember me</label>
  			</div>
            <button type="submit" class="btn btn-primary form-control" >Sign In</button>
            {{ csrf_field()}}
        </form>
    </div>
</div>
@endsection

@section("scripts")
<script type="text/javascript">  
	$('#rememberme2').change(function() {
  		if($(this).prop('checked')) {
  			$('#rememberme').val('yes');
  		} else {
  			$('#rememberme').val('no');
  		}
	});
</script>
@endsection