<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageProduct extends Model
{
	protected $table = 'imageproducts';
    protected $fillable = [
    	'filename',
    	'product_id'
    ];
}
