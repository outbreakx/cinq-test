<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageRetailer extends Model
{
	protected $table = 'imageretailers';
	
	protected $fillable = [
    	'filename',
    	'retailer_id'
    ];
}
