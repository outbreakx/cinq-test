<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ImageRetailer;
use App\Retailer;

class Retailer extends Model
{
    protected $fillable = [
    	'name',
    	'description',
    	'website',
        'user_id'
    ];
    public function logo() {
    	return $this->hasOne('App\ImageRetailer', 'retailer_id')->select(['id', 'filename']);
    }
    public function products () {
    	return $this->hasMany('App\Product', 'retailer_id');
    }
}
