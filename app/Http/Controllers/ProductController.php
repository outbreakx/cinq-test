<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Retailer;

class ProductController extends Controller
{
    public function __construct() 
    {
        //middleware stuff.
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product.index', ['products' => $products ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.page', ['product' => $product]);
    }

    public function new_product(Request $request) {
        $retailers = Retailer::all('id', 'name');
        return view('product.create', ['retailers' => $retailers]);
    }

    public function edit_product(Request $request, Product $product) {
        return view('product.edit', ['product' => $product]);
    }
}
