<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class SendEmailController extends Controller
{
    //

    public function sendemail(Request $request, $id) {
    	$request->validate([
    		'email' => 'email|required'
    	]);

    	$product = Product::find($id);
    	/*
        $obj = new \stdClass();
        $obj->text = 'blah blah';
        $obj->sender = 'test';
        $obj->receiver = 'test';

        Mail::to($request->input('email'))->send(new EmailClass($obj));*/

    	return response()->json(['status' => 'success', 'message' => 'email has been sent!'], 200);
    }
}
