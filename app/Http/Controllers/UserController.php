<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class UserController extends Controller
{
    public function getSignUp() {
        return view('user.signup');
    }
    public function postSignUp(Request $request) {

        // validate the input.
        $request->validate([
            'username' => 'required|min:6',
            'email' => 'email|required|unique:users',
            'password' => 'required|min:4'
        ]);

        // create new user.
        $user = new User([
            'name' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ]);
        $user->save();
        
        // login current user.
        Auth::login($user);

        // go to the user profile.
        return redirect()->route('user.profile');
    }

    public function getSignIn() {
        return view('user.signin');
    }
    public function postSignIn(Request $request) {
        $this->validate($request,[
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);
        
        $remember = $request->input('rememberme') == 'yes' ? true : false;
        
        if (Auth::attempt(['email' => $request->input('email'),'password' => $request->input('password')], $remember))
        {
            return redirect()->route('home');
        }
        return redirect()->back()->withErrors(['failed_attempt' => "The email doesn't exist or the password is wrong."]);
    }

    public function getProfile() {
        return view('user.profile');
    }
    public function getLogout() {
        Auth::logout();
        return redirect()->back();
    }
}
