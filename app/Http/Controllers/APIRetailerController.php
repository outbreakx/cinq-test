<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Retailer;
use App\ImageRetailer;

class APIRetailerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retailers = Retailer::all();

        if(count($retailers)) {
            return response()->json(['data' => $retailers, 'status' => 'success'], 200);
        }
        return response()->json(['data' => null, 'status' => 'success', 'message' => 'There are no retailers'], 204);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:64',
            'description' => 'required|max:512',
            'website' => 'required|max:128', 
            'user_id' => 'required',

            'filename' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $retailer = Retailer::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'website' => $request->input('website'),
            'user_id' => $request->input('user_id')
        ]);
        $paths  = [];
        if($request->hasfile('filename')) 
        {
            $image_path = 'images/retailers/' . $retailer->id . '/';
            $storage_path = storage_path() . "/app/public/" . $image_path;
            foreach($request->file('filename') as $image)
            {
                $filename= $image->getClientOriginalName();
                $image->move($storage_path , $filename);  

                $path = "storage/" . $image_path . $filename;
                array_push($paths, $path); 
                $retailerImage = ImageRetailer::create([
                    'filename' => $path,
                    'retailer_id' => $retailer->id
                ]);
            }
        }
        return response()->json($paths, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Retailer $retailer)
    {
        if($retailer) 
        {
            return response()->json(['data' => $retailer, 'status' => 'success', 'message' => 'succefully got the retailer.'], 200);
        }
        return response()->json(['data' => null, 'status' => 'failed', 'message' => 'Resource not found'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Retailer $retailer)
    {
        if(!$retailer) {
            return response()->json(['data' => null, 'status' => 'failed', 'message' => 'Resource not found'], 404);
        }
        $request->validate([
            'name' => 'required|max:64',
            'description' => 'required|max:512',
            'website' => 'required|max:128', 
        ]);
        $retailer->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'website' => $request->input('website'),
        ]);
        return response()->json(['data' => $retailer, 'status' => 'success', 'message' => 'Resource has been updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Retailer $retailer)
    {
        if(!$retailer) {
            return response()->json(['data' => null, 'status' => 'failed', 'message' => 'Resource not found'], 404);
        }
        $retailer->delete();
        return response()->json(['data' => null, 'status' => 'success', 'message' => 'Resource has been deleted!'], 204);
    }
}
