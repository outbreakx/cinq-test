<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ImageProduct;

class APIProductController extends Controller
{
    //
    public function __construct() 
    {
        //middleware stuff.
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        if(count($products)) {
            return response()->json(['data' => $products, 'status' => 'success'], 200);
        }
        return response()->json(['data' => null, 'status' => 'success', 'message' => 'There are no products'], 204);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:64',
            'description' => 'required|max:512',
            'price' => 'required', 
            'retailer_id' => 'required',

            'filename' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        // create product.
        // 
        

        $product = Product::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'retailer_id' => $request->input('retailer_id')
        ]);
        $paths  = [];
        if($request->hasfile('filename')) 
        {
            $image_path = 'images/products/' . $product->id . '/';
            $storage_path = storage_path() . "/app/public/" . $image_path;
            foreach($request->file('filename') as $image)
            {
                $filename= $image->getClientOriginalName();
                $image->move($storage_path , $filename);  

                $path = "storage/" . $image_path . $filename;
                array_push($paths, $path); 
                $productImage = ImageProduct::create([
                    'filename' => $path,
                    'product_id' => $product->id
                ]);
            }
        }
        return response()->json($paths, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        if($product) 
        {
            return response()->json(['data' => $product, 'status' => 'success', 'message' => 'succefully got the product.'], 200);
        }
        return response()->json(['data' => null, 'status' => 'failed', 'message' => 'Resource not found'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if(!$product) {
            return response()->json(['data' => null, 'status' => 'failed', 'message' => 'Resource not found'], 404);
        }
        $request->validate([
            'name' => 'required|max:64',
            'description' => 'required|max:512',
            'price' => 'required', 
        ]);
        $product->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
        ]);
        return response()->json(['data' => $product, 'status' => 'success', 'message' => 'Resource has been updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if(!$product) {
            return response()->json(['data' => null, 'status' => 'failed', 'message' => 'Resource not found'], 404);
        }
        $product->delete();
        return response()->json(['data' => null, 'status' => 'success', 'message' => 'Resource has been deleted!'], 204);
    }
}
