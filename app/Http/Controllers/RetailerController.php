<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Retailer;
use App\User;

class RetailerController extends Controller
{
    public function __construct() 
    {
        //middleware stuff.
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retailers = Retailer::all();
        return view('retailer.index', ['retailers' => $retailers ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Retailer $retailer)
    {
        return view('retailer.page', ['retailer' => $retailer]);
    }


    public function new_retailer(Request $request) {
        $users = User::all('id', 'name');
        return view('retailer.create', ['users' => $users]);
    }

    public function edit_retailer(Request $request, Retailer $retailer) {
        return view('retailer.edit', ['retailer' => $retailer]);
    } 
}
