<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    	'name',
    	'price',
    	'description',
    	'retailer_id'
    ];


    public function images() {
    	return $this->hasMany('App\ImageProduct', 'product_id')->select(['id', 'filename']);
    }
    public function retailer() {
    	return $this->belongsTo('App\Retailer', 'retailer_id')->select(['name']);
    }
}
