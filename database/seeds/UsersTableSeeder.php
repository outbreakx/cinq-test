<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Retailer;
use App\ImageRetailer;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // lets make a user.

        $user = User::all()->first();
        $imgs = ["storage/images/150x150.png", "storage/images/200x200.png", "storage/images/250x250.png"];

        if($user == null) {
            $user = User::create([
                'name' => "test",
                'email' => 'test@gmail.com',
                'password' => bcrypt('12345'),
            ]);
            // make the user, also a retailer.
            $retailer = Retailer::create([
                'name' => "Retailer test",
                'description' => 'retailer description',
                'website' => 'https://www.github.com/bufige',
                'user_id' => $user->id
            ]);
            // create a logo for the retailer.
            $imageRetail = ImageRetailer::create([
                'filename' => $imgs[2],
                'retailer_id' => $retailer->id
            ]);
        }
    }
}
