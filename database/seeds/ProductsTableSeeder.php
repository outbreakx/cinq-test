<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\ImageProduct;
use App\User;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $imgs = ["storage/images/150x150.png", "storage/images/200x200.png", "storage/images/250x250.png"];

    	$user = User::all()->first();
    	for ($i = 1; $i < 6; $i++) {
    		
            $product = Product::create([
                'name' => "product-" . $i,
                'description' => 'this is product ' . $i,
                'price' => 100 * $i,
                'retailer_id' => $user->getRetailer->id
            ]);

            for($k = 0; $k < 3; $k++) {
            	$productImage = ImageProduct::create([
                	'filename' => $imgs[array_rand($imgs, 1)],
                	'product_id' => $product->id
            	]);
            }
        }
    }
}
