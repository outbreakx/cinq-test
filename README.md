# Follow the steps ahead to use the application

## Server Configuration
#####Copy .env.example to .env and change the following variables.
- DB_DATABASE=phptest
- DB_USERNAME=root
- DB_PASSWORD=12345

## Run app

Run all the following commands.

- composer install
- npm install
- php artisan migrate
- php artisan db:seed
- php artisan key:generate
- php artisan serve

### List of API endpoints

1. Product API Endpoints
    - GET => '/api/products' - list with all products

    - POST => '/api/products' - Store a new product

    - GET => '/api/products/{product}' - Get product details

    - PUT => '/api/products/{product}', - Update a product

    - DELETE => '/api/products/{product}' - Delete a product

2. Retailer API Endpoints

    - GET => '/api/retailers' - list with all products

    - POST => '/api/retailers' - Store a new retailer

    - GET => '/api/retailers/{retailer}' -  Get retailer details

    - PUT => '/api/retailers/{retailer}', - Update a retailer

    - DELETE => '/api/retailers/{retailer}' - Delete a retailer